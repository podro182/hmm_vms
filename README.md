# Stochastic Modelling and Data Analysis of Fishery Data

Disturbance of the benthos caused by bottom trawling renders fine 
spatio-temporal estimates of fishing activity vital for environmental 
management and assessment. Vessel monitoring system (VMS) provides high 
resolution data that can help the estimation of fishing activity. The 
estimation is performed by the binary classification of each of the sequential 
VMS positions into fishing or non-fishing. The objective of the present thesis 
is to compare and improve existing classification methods. The methods 
presented are simple mixture models, hidden Markov models and hidden 
semi-Markov models, with covariates the speed and in some cases the time of day 
provided by the VMS. A novel labeling strategy is developed which describes 
efficiently the mixture components that correspond to fishing. The methods are 
applied to VMS data from 6430 trips of Swedish trawlers and compared, based on 
their performance, on pre-classified VMS data from 10 trips of Danish trawlers.